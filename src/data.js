const Product_descriptions=[{
    id:1,
    image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/products/TTC_0024_Face-Brightening-Daily-cleanser-1_1_400x.png?v=1615368951",
    heading:"Daily cleanser",
    para:"Our “Face Brightening Daily Cleanser” is made with 12 organic tribal forest sourced ingredients which are amazing for skin care. It helps in skin brightening, getting an even tone, for spot reduction, tan removal, blackhead removal and imparts a young, vivacious and clear skin."
},
{
    id:2,
    image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/products/d78708ff-b170-4e93-a52d-ad6bcac8e8da_900x.png?v=1630174768",
    heading:"Face Brightening kit",
    para:"Here’s the Best Combo that helps you get an Even Toned, Blemish Free, Brighter & Glowing Skin. With the regular use of the “FACE BRIGHTENING KIT”, you can experience that fresh, young and vivacious look you’ve always wanted. It helps to efficiently clear dark patches, dark circles, blemishes, and makes you look all glammed up."
},
{
    id:3,
    image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/products/TTC_0031_Organic-Hair-Cleanser-2_900x.png?v=1615368912",
    heading:"Organic hair cleanser",
    para:"Our “Organic Hair Cleanser” is a natural Shampoo that effectively cleans your scalp and hair removing dirt, debris and excess oils while letting all the essential nutrients intact and making your hair soft and manageable. The Cleanser on regular use helps restore natural hair colour and makes the hair healthy from the roots till the ends.."
},
{
    id:4,
    image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/products/TTC_0008_90-Day-Miracle-Oil-2_900x.png?v=1613998714",
    heading:"90 Day Miracle Hair Oil",
    para:"Our “Face Brightening Daily Cleanser” is made with 12 organic tribal forest sourced ingredients which are amazing for skin care. It helps in skin brightening, getting an even tone, for spot reduction, tan removal, blackhead removal and imparts a young, vivacious and clear skin."
},
{
    id:5,
    image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/products/d78708ff-b170-4e93-a52d-ad6bcac8e8da_900x.png?v=1630174768",
    heading:"Face brightnening kit",
    para:"Here’s the Best Combo that helps you get an Even Toned, Blemish Free, Brighter & Glowing Skin. With the regular use of the “FACE BRIGHTENING KIT”, you can experience that fresh, young and vivacious look you’ve always wanted. It helps to efficiently clear dark patches, dark circles, blemishes, and makes you look all glammed up.."
},
{
    id:6,
    image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/products/TTC_0014_ANTI-ACNE-TREATMENT-KIT_900x.png?v=1615369440",
    heading:"Anti-Acne treatment kit",
    para:"Acne is one of the most common skin conditions anywhere across the World. Try out our “Anti-Acne Treatment Kit”, which helps to effectively fight against pimples, acne, acne scars & spots or blemishes, and fades them away from within."
}]

const Product_ingredient=[{
        unique_id:1,
        image:"https://www.24mantra.com/wp-content/uploads/2018/07/Proven-Health-Benefits-of-Turmeric-Powder.png",
        span1:"TURMERIC",
        span2:"- powder",
        para:"Turmeric is a common spice that comes from the root of Curcuma longa. It contains a chemical called curcumin, which might reduce swelling."
},
{
    unique_id:2,
        image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/SANDALWOOD_1_160x160.png?v=1613028417",
        span1:"SANDALWOOD",
        span2:"- powder",
        para:"The wonder wood is ground into a powder and steam-distilled into oil for making soaps, cosmetics, candles, incense, medicine and perfumes. Sandalwood oil is extensively used in beauty products to enhance skin health"
},
{
    unique_id:3,
        image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/ROSE-PETALS_160x160.png?v=1613026358",
        span1:"ROSEPETALS",
        span2:"- powder",
        para:"Rose petal powder is like an elixir. It can be used in various wonderful ways, giving innumerable benefits to the skin and hair. Due to its soothing fragrance, rose is said to balance 'vata dosha'. This dosha, according to Ayurveda, manifests mainly as anxiety, insomnia, and digestive issues."
},
{
        unique_id:4,
        image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/MANJISTHA_1_160x160.png?v=1613028524",
        span1:"MANGISTHA",
        span2:"- powder",
        para:"Manjistha has antioxidant, antimicrobial, and anti-inflammatory properties. It is known as the essential remedy for skin care in Ayurveda. It has raktashodhak (blood purifying) properties that make it beneficial for the circulatory system. This herb helps remove toxins from your blood and keep infections at bay. One can reduce all signs of ageing like wrinkling, blemishes, fine lines, dark circles, spots, and scars with this herb."
}]
const Footer_page=[{
    id:1,
    heading:"What",
    para:"The Tribe Concepts, India's most trusted organic skin care and hair care brand that believes in the power of nature and the notes of Ayurveda."
  },
  {
    id:2,
    heading:"HOW",
    para:"Here's how The Tribe Concept’s 'Brightening Daily Cleanser' is made with 12 organic tribal forest sourced ingredients which are amazing for skin care. It helps in skin brightening, getting a lighter and even tone, for spot reduction, tan removal, blackhead removal and imparts a young, vivacious and glowing skin."},
  {
    id:3,
    heading:"Why",
    para:"Keep it up Tribe concept . The packaging of the product is so classy and eco friendly . Also comes with a spoon for convenience . The consistency is so good upon mixing wid water . Do give it a try ."
  },
  {
    id:4,
    heading:"Who",
    para:"Founded by women, we embarked on a journey to provide safe and clean beauty, to bring about substantial changes in the beauty & wellness sphere. We have advocated the ‘women in business’ notion whilst giving support to women at all levels - from handpicking the ingredients to shape up a product, women have been a quintessential part of the journey.  80% of The Tribe Concepts’ team & board are women."
  }
  ]
const Environments=[
    { 
      id:1,
      image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/Indian-Ingredients.png?v=1612947951",
      heading:"Indian Indegeredients",
      para:"Each of our ingredients is sustainably sourced from tribal valleys of India where the soil is untampered with, and the air is as pure as it gets."
      
      
  
    },
    {
      id:2,
      image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/No-added-chemicals.png?v=1612947959",
      heading:"No Added Chemicals",
      para:"chemical-free is a term used in marketing to imply that a product is safe, healthy or environmentally friendly because it only contains natural ingredients"
    },
    {
      id:3,
      image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/Sustainability.png?v=1612947976",
      heading:"Sustainability",
      para:"Sustainability means meeting our own needs without compromising the ability of future generations to meet their own needs. In addition to natural resources, we also need social and economic resources"
    },
    {
      id:4,
      image:"https://cdn.shopify.com/s/files/1/0284/5599/3449/files/Vegan-_-Cruelty-free.png?v=1612947985",
      heading:"Vegan & Cruetly free",
      para:"Cruelty-free means that the product was developed without any tests on animals, while vegan means that the product does not include any animal-derived ingredients"
    }
  ]



export {Product_descriptions,Product_ingredient,Footer_page,Environments}