
import React, { Component } from "react";
import '../../App.css'
import Footer from "./footer";

class Foooter extends Component{
  render(){
    const {eachlink} = this.props
    return(
        
            <div className="col four left">
              <h1>{eachlink.heading}</h1>
              <p>
                {eachlink.para}
              </p>
             
            </div>
    )
  }
}

export default Foooter
