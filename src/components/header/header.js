
import '../../App.css';
import React, { Component } from "react";


class Header extends Component {
  render() {
    return (

        <div className="hold">
        <div className="header">
          <div className="container">
            <div className="logo1">
              <img src="https://cdn.shopify.com/s/files/1/0284/5599/3449/files/TTC_Logos_with_TM-01_100x@2x.png?v=1640326478" />
            </div>
            <ul className="nav">
              <li>
                <a href="/#">Categories</a>
              </li>
              <li>
                <a href="/#">Wishlist</a>
              </li>
              <li>
                <a href="/#">Cart</a>
              </li>
              <li>
                <a href="/#">Profile</a>
              </li>
            </ul>
          </div>
        </div>
      </div>        
    )}}
export default Header