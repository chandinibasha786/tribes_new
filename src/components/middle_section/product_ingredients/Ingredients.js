
import React, { Component } from "react";
import '../../../App.css'

class Ingredient extends Component{
  render(){
    const {eachlinks} = this.props
    return(
        <div className="col two bg margin extrapad">
            <h1 className="icon side"><img src={eachlinks.image} className='image4'/></h1>
            <span className="feature side">{eachlinks.span1}</span>
            <span className="side"> {eachlinks.span2}</span>
            <p className="side">
            {eachlinks.para}
            </p>
          </div>
    )
  }
}

export default Ingredient