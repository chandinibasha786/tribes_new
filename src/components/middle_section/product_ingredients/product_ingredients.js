
import '../../../App.css';
import React, { Component } from "react";
import Ingredient from './Ingredients';

class Product_ingredients extends Component {
  render() {
    const {Product_ingredient}= this.props
    return (
        <div className="section bg">
        <div className="container ">
          <h1>Ingredients</h1>
          <h2>Natural Ingredients</h2>
          {Product_ingredient.map((link) => (
          <Ingredient  eachlinks={link}/>
          ))}
          <div className="group margin" />
          <div className="group" />
        </div>
      </div>      
    )}}
export default Product_ingredients


