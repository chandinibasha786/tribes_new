
import '../../../App.css';
import React, { Component } from "react";
import Environments_des from './enviroment_des';

class Environment extends Component {
  render() {
    const {Environments}=this.props
    return (

        <div className="section">
        <div className="container">
          <h1>Environment</h1>
          {Environments.map((link) => (
          <Environments_des  eachlink={link}/>
          ))} 

          <div className="group" />
        </div>
      </div>      
    )}}
export default Environment