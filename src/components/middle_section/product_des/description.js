
import React, { Component } from "react";
import '../../../App.css'

class Products extends Component{
  render(){
    const {eachlink} = this.props
    return(
        <div className="col three">
        <h1 className="icon side"><img src={eachlink.image}className='image3'/></h1>
        <h1 className="feature side">{eachlink.heading}</h1>
        <p className="side">
        {eachlink.para}
        </p>
      </div>
    )
  }
}

export default Products



    